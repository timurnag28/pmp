import React from 'react';
// import CertificateScreen from '../screens/certificates/CertificateScreen';
import SettingsScreen from '../screens/settingsSrc/SettingsScreen';
import {createStackNavigator} from 'react-navigation-stack';
import {ImageBackground} from 'react-native';
import {NavigationProps} from '../share/interfaces';

const MainStack = createStackNavigator(
  {
    // CertificatesScreen: {
    //   screen: CertificateScreen,
    // },
    SettingsScreen: {
      screen: SettingsScreen,
    },
  },
  {
    initialRouteName: 'SettingsScreen',
    headerMode: 'none',
    defaultNavigationOptions: {
      cardStyle: {
        backgroundColor: 'transparent',
      },
    },
  },
);

export default class MainStackContainer extends React.Component<
  NavigationProps
> {
  static router = MainStack.router;

  render() {
    return (
      <ImageBackground
        style={{
          width: '100%',
          height: '100%',
        }}
        source={require('../../assets/images/backgroundTracery.png')}>
        <MainStack navigation={this.props.navigation} />
      </ImageBackground>
    );
  }
}

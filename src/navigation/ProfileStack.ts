import {createStackNavigator} from 'react-navigation-stack';
import Schedule from '../screens/profile/Schedule';
import Profile from '../screens/profile/Profile';
import Statistics from '../screens/profile/Statistics';
import FoodDiary from '../screens/profile/foodDiary/FoodDiary';
import AddDish from '../screens/profile/addDish/AddDish';
import CertificateScreen from '../screens/certificates/CertificateScreen';

export default createStackNavigator(
  {
    Profile: {
      screen: Profile,
    },
    Schedule: {
      screen: Schedule,
    },
    Statistics: {
      screen: Statistics,
    },
    FoodDiary: {
      screen: FoodDiary,
    },
    AddDish: {
      screen: AddDish,
    },
    CertificatesScreen: {
      screen: CertificateScreen,
    },
  },
  {
    initialRouteName: 'Profile',
    headerMode: 'none',
    defaultNavigationOptions: {
      cardStyle: {
        backgroundColor: '#FAFAFA',
      },
    },
  },
);

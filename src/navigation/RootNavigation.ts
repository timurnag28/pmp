import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import MainStack from './MainStack';
import ProfileStack from './ProfileStack';

const AppNavigator = createSwitchNavigator(
  {
    mainStack: MainStack,
    profileStack: ProfileStack,
  },

  {
    initialRouteName: 'profileStack',
  },
);
const AppContainer = createAppContainer(AppNavigator);
export default AppContainer;

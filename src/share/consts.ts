import {Dimensions} from 'react-native';

export const WINDOW_WIDTH = Dimensions.get('window').width;
export const WINDOW_HEIGHT = Dimensions.get('window').height;
export const HEADER_HEIGHT = 50;
export const ROW_ICONS_HEIGHT = WINDOW_HEIGHT / 18;
export const fontSize12 = WINDOW_WIDTH / 34;
export const fontSize14 = WINDOW_WIDTH / 30;
export const fontSize16 = WINDOW_WIDTH / 26;
export const fontSize18 = WINDOW_WIDTH / 23;
export const fontSize20 = WINDOW_WIDTH / 21;
export const fontSize24 = WINDOW_WIDTH / 18;
export const fontSize28 = WINDOW_WIDTH / 15;
export const fontSize32 = WINDOW_WIDTH / 13.5;
export const fontSize34 = WINDOW_WIDTH / 13;
export const fontSize36 = WINDOW_WIDTH / 12;

export const PfDinDisplayProBold = 'PF-DinDisplay-Pro-Bold';
export const PfDinDisplayPro = 'PF-DinDisplay-Pro';
export const BadScript = 'Bad-Script';
export const SfUiDisplayRegular = 'SF-UI-Display-Regular';

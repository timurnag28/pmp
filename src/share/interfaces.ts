import {NavigationScreenProp, NavigationState} from 'react-navigation';

export interface NavigationProps {
  navigation: NavigationScreenProp<NavigationState>;
}
export interface IGiftPrices {
  price: number;
}
export interface IMenuItems {
  title: string;
  screen: string;
}

import React, {Component} from 'react';
import {
  Text,
  View,
  StyleProp,
  ViewStyle,
  TextStyle,
  Animated,
} from 'react-native';

import {SafeAreaView} from 'react-navigation';
import {
  fontSize18,
  fontSize20,
  HEADER_HEIGHT,
  PfDinDisplayProBold,
} from '../consts';

interface HeaderProps {
  title?: string;
  style?: StyleProp<ViewStyle>;
  backFunction?: () => void;
  backTitle?: string;
  animated?: boolean;
  textStyle?: StyleProp<TextStyle>;
  titleStyle?: StyleProp<ViewStyle>;
  headerRight?: object;
  headerLeft?: object;
  marginTopHeader?: number;
}

export default class Header extends Component<HeaderProps> {
  render() {
    const {
      animated,
      style,
      title,
      titleStyle,
      headerLeft,
      headerRight,
      marginTopHeader,
    } = this.props;
    const ViewComp = animated ? Animated.View : View;
    return (
      <SafeAreaView
        style={[
          {
            backgroundColor: 'transparent',
            zIndex: 100,
            width: '100%',
          },
          style,
        ]}>
        <View
          style={{
            marginTop: marginTopHeader,
            overflow: 'hidden',
            height: HEADER_HEIGHT,
          }}>
          {headerLeft ? (
            <View
              style={[
                {
                  position: 'absolute',
                  top: 0,
                  bottom: 0,
                  left: 0,
                  justifyContent: 'center',
                  alignSelf: 'center',
                },
                titleStyle,
              ]}>
              {headerLeft}
            </View>
          ) : null}
          <ViewComp
            style={[
              {
                position: 'absolute',
                bottom: 0,
                top: 0,
                justifyContent: 'center',
                alignSelf: 'center',
              },
              titleStyle,
            ]}>
            <Text
              style={{
                fontFamily: PfDinDisplayProBold,
                color: '#151C26',
                fontSize: fontSize20,
              }}>
              {title}
            </Text>
          </ViewComp>
          {headerRight ? (
            <View style={{position: 'absolute', right: 0, bottom: 0, top: 6}}>
              {headerRight}
            </View>
          ) : null}
        </View>
      </SafeAreaView>
    );
  }
}

import React from 'react';
import {TextInput} from 'react-native';
import {fontSize18, PfDinDisplayPro} from '../consts';

export const TextInputCustom = (props: any) => {
  return (
    <TextInput
      {...props} // Inherit any props passed to it; e.g., multiline, numberOfLines below
      editable
      style={{
        fontFamily: PfDinDisplayPro,
        color: '#000000',
        fontSize: fontSize18,
        paddingLeft: 0,
      }}
    />
  );
};

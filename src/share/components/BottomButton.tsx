import {StyleSheet, Text, TouchableOpacity} from 'react-native';
import React from 'react';
import {fontSize16, PfDinDisplayPro, WINDOW_HEIGHT} from '../consts';

interface IBottomButton {
  title: string;
  backFunction: () => void;
}
export const BottomButton = ({title, backFunction}: IBottomButton) => {
  return (
    <TouchableOpacity onPress={backFunction} style={styles.bottomButton}>
      <Text style={styles.bottomButtonTitle}>{title}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  bottomButton: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    height: WINDOW_HEIGHT / 12,
    borderTopLeftRadius: 16,
    borderTopRightRadius: 16,
    backgroundColor: '#F46F22',
    elevation: 6,
  },
  bottomButtonTitle: {
    color: '#FFFFFF',
    fontSize: fontSize16,
    fontFamily: PfDinDisplayPro,
  },
});

import React from 'react';
import {StyleSheet, View} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {WINDOW_HEIGHT} from '../consts';

export default function BackGroundGradient() {
  return (
    <View style={styles.linearGradient}>
      <LineGradient angle={180} location={[0, 1]} />
    </View>
  );
}

const LineGradient = ({
  angle,
  location,
}: {
  angle: number;
  location: [number, number];
}) => {
  return (
    <LinearGradient
      useAngle
      angle={angle}
      angleCenter={{x: 0.5, y: 0.5}}
      locations={location}
      colors={['rgba(0,0,0,0)', '#000000']}
      style={styles.linearGradient}
    />
  );
};

const styles = StyleSheet.create({
  linearGradient: {
    height: WINDOW_HEIGHT / 4,
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    borderRadius: 8,
  },
});

import React, {Component} from 'react';
import {
  ScrollView,
  SectionList,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
// @ts-ignore
import {Calendar, LocaleConfig} from 'react-native-calendars';
import Header from '../../share/components/Header';
import {
  fontSize16,
  fontSize28,
  fontSize34,
  PfDinDisplayPro,
  PfDinDisplayProBold,
  WINDOW_HEIGHT,
} from '../../share/consts';
import EntypoIcon from 'react-native-vector-icons/Entypo';
import {NavigationProps} from '../../share/interfaces';
import LinearGradient from 'react-native-linear-gradient';
const DATA = [
  {
    title: 'Планы на сегодня',
    data: [
      'Персональная тренировка 55 минут',
      'СПА процедуры',
      'Посещение косметолога',
    ],
  },
  {
    title: 'Планы на 11.11.2019',
    data: ['Прогулка в парке'],
  },
];

LocaleConfig.locales['ru'] = {
  monthNames: [
    'Январь',
    'Февраль',
    'Март',
    'Апрель',
    'Май',
    'Июнь',
    'Июль',
    'Август',
    'Сентябрь',
    'Октябрь',
    'Ноябрь',
    'Декабрь',
  ],
  monthNamesShort: [
    'Янв.',
    'Фев.',
    'Мрт.',
    'Апр.',
    'Май',
    'Ин.',
    'Ил.',
    'Авг.',
    'Снт.',
    'Окт.',
    'Нб.',
    'Дек.',
  ],
  dayNames: [
    'Понедельник',
    'Вторник',
    'Среда',
    'Четверг',
    'Пятница',
    'Суббота',
    'Воскресенье ',
  ],
  dayNamesShort: ['ВС', 'ПН', 'ВТ', 'СР', 'ЧТ', 'ПТ', 'СБ'],
};
LocaleConfig.defaultLocale = 'ru';
export default class Schedule extends Component<NavigationProps> {
  constructor(props: any) {
    super(props);
    this.state = {
      dateSelected: {},
    };
  }

  render() {
    return (
      <View>
        <Header
          style={styles.header}
          marginTopHeader={WINDOW_HEIGHT / 16}
          headerLeft={
            <TouchableOpacity
              style={{paddingRight: 16}}
              onPress={() => {
                this.props.navigation.goBack();
              }}>
              <EntypoIcon
                name="chevron-small-left"
                size={fontSize34}
                color="#151C26"
              />
            </TouchableOpacity>
          }
          title={'Мое расписание'}
        />
        <ScrollView>
          <View style={styles.container}>
            <LinearGradient
              useAngle
              angle={267}
              locations={[0, 1]}
              colors={['rgba(255, 255, 255, 0.56);', '#DADADA']}
              style={styles.calendarGradient}>
              <Calendar
                style={styles.calendar}
                // @ts-ignore
                markedDates={this.state.dateSelected}
                renderArrow={(direction: string) => customIcons(direction)}
                theme={calendarTheme}
                onDayPress={(day: any) => {
                  this.setState({
                    dateSelected: {
                      [day.dateString]: {
                        selected: true,
                      },
                    },
                  });
                }}
                monthFormat={'MMMM yyyy'}
                hideExtraDays={true}
                firstDay={1}
              />
            </LinearGradient>
            <View style={styles.containerListFood}>
              <SectionList
                scrollEnabled={false}
                showsVerticalScrollIndicator={false}
                style={styles.listDish}
                sections={DATA}
                keyExtractor={(item, index) => item + index}
                renderItem={({item, index}) => (
                  <LinearGradient
                    key={index}
                    useAngle
                    angle={240}
                    locations={[0, 1]}
                    colors={['rgba(255, 255, 255, 0.5);', '#DADADA']}
                    style={styles.gradient}>
                    <View style={{flexDirection: 'column', paddingLeft: 16}}>
                      <Text
                        style={{
                          fontSize: fontSize16,
                          fontFamily: PfDinDisplayPro,
                        }}>
                        {item}
                      </Text>
                      <Text style={{color: 'rgba(30, 32, 34, 0.54)'}}>12:00 - 15:00</Text>
                    </View>
                    <EntypoIcon name="leaf" size={fontSize28} color="#0FD8D8" />
                  </LinearGradient>
                )}
                renderSectionHeader={({section: {title}}) => (
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                    }}>
                    <Text
                      style={{
                        fontFamily: PfDinDisplayProBold,
                        fontSize: 18,
                        paddingVertical: 18,
                      }}>
                      {title}
                    </Text>
                    <TouchableOpacity onPress={() => alert('.')}>
                    <Text
                      style={{
                        fontFamily: PfDinDisplayPro,
                        fontSize: 18,
                        paddingVertical: 18,
                        color: '#F46F22',
                      }}>
                      ВСЕ
                    </Text>
                    </TouchableOpacity>
                  </View>
                )}
              />
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {flex: 1, alignItems: 'center', marginBottom: 64},
  calendarGradient: {
    marginVertical: 24,
    width: '96%',
    borderRadius: 10,
    alignItems: 'center',
  },
  calendar: {
    width: '100%',
    borderRadius: 10,
    backgroundColor: 'transparent',
  },
  header: {
    borderBottomWidth: 1,
    borderBottomColor: 'rgba(0, 0, 0, 0.12)',
  },
  gradient: {
    marginTop: 8,
    paddingVertical: 18,
    paddingHorizontal: 8,
    borderRadius: 10,
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  listDish: {
    marginBottom: WINDOW_HEIGHT / 11.5,
    backgroundColor: '#FAFAFA',
    width: '100%',
    paddingHorizontal: 16,
    borderTopLeftRadius: 16,
    borderTopRightRadius: 16,
  },
  containerListFood: {
    backgroundColor: '#FAFAFA',
    width: '100%',
    flex: 1,
    borderRadius: 16,
  },
});

const calendarTheme = {
  calendarBackground: 'transparent',
  textDayFontFamily: PfDinDisplayPro,
  textMonthFontFamily: PfDinDisplayProBold,
  textDayHeaderFontFamily: PfDinDisplayProBold,
  dayTextColor: '#151C26',
  todayTextColor: '#F46F22',
  textSectionTitleColor: '#151C26',
  monthTextColor: '#151C26',
  selectedDayBackgroundColor: 'rgba(244, 111, 34, 0.12)',
  selectedDayTextColor: '#F46F22',
  textDayFontSize: fontSize16,
  'stylesheet.day.basic': {
    text: {
      marginTop: 6,
    },
  },
};

const customIcons = (direction: string) => {
  {
    if (direction === 'right') {
      return (
        <EntypoIcon
          name="chevron-small-right"
          size={fontSize28}
          color="rgba(21, 28, 38, 0.5)"
        />
      );
    } else {
      return (
        <EntypoIcon
          name="chevron-small-left"
          size={fontSize28}
          color="rgba(21, 28, 38, 0.5)"
        />
      );
    }
  }
};

import React, {Component} from 'react';
import {
  ImageBackground,
  ScrollView,
  SectionList,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {AnimatedCircularProgress} from 'react-native-circular-progress';
import {BottomButton} from '../../../share/components/BottomButton';
import {NavigationProps} from '../../../share/interfaces';
import {
  fontSize12,
  fontSize14,
  fontSize16,
  fontSize18,
  fontSize28,
  fontSize34,
  PfDinDisplayPro,
  PfDinDisplayProBold,
  SfUiDisplayRegular,
  WINDOW_HEIGHT,
} from '../../../share/consts';
import store from '../../../stores';
import {observer} from 'mobx-react';
import Header from '../../../share/components/Header';
import EntypoIcon from 'react-native-vector-icons/Entypo';
import Carousel from 'react-native-snap-carousel';
import LinearGradient from 'react-native-linear-gradient';
import {SubstanceElement} from './components/SubstanceElement';

const DATA = [
  {
    title: 'Завтрак',
    data: ['Салат', 'Суп', 'Рыба'],
  },
  {
    title: 'Обед',
    data: ['Яичница с колбасой', 'Стейк', 'Салат'],
  },
  {
    title: 'Ужин',
    data: ['Рис', 'Салат', 'Курица'],
  },
];

const iconImages = {
  prot: require('../../../../assets/images/icons/prot.png'),
  uglevod: require('../../../../assets/images/icons/uglevod.png'),
  fats: require('../../../../assets/images/icons/fats.png'),
};

interface IState {
  activeElement: number;
}

@observer
export default class FoodDiary extends Component<NavigationProps, IState> {
  currIndex = 0;
  constructor(props: any) {
    super(props);
    this.state = {
      activeElement: 0,
    };
  }

  componentDidMount() {
    // @ts-ignore
    this.currIndex = this._carousel.currentIndex;
  }

  _renderItem = ({item, index}: {item: string; index: number}) => {
    return (
      <View
        style={{
          backgroundColor: index === this.currIndex ? '#F46F22' : 'transparent',
          borderRadius: 30,
          paddingVertical: 8,
        }}>
        <Text
          style={{
            color: index === this.currIndex ? 'white' : 'black',
            textAlign: 'center',
          }}>
          {item}
        </Text>
      </View>
    );
  };

  render() {
    const {
      // dish,
      calories,
      // selectedEating
    } = store.addDishStore;
    return (
      <ImageBackground
        style={{
          width: '100%',
          height: '100%',
        }}
        source={require('../../../../assets/images/backLeftManImage.png')}>
        <ScrollView>
          <View style={styles.container}>
            <Header
              marginTopHeader={WINDOW_HEIGHT / 16}
              headerLeft={
                <TouchableOpacity
                  style={{paddingRight: 16}}
                  onPress={() => {
                    this.props.navigation.goBack();
                  }}>
                  <EntypoIcon
                    name="chevron-small-left"
                    size={fontSize34}
                    color="#151C26"
                  />
                </TouchableOpacity>
              }
              title={'Дневник питания'}
            />
            <View style={{height: WINDOW_HEIGHT / 8}}>
              <Carousel
                shouldOptimizeUpdates={true}
                enableMomentum={true}
                activeSlideOffset={10}
                callbackOffsetMargin={20}
                ref={(c: any) => {
                  // @ts-ignore
                  this._carousel = c;
                }}
                onBeforeSnapToItem={currentIndex => {
                  // this.setState({activeElement: currentIndex});
                  this.currIndex = currentIndex;
                  this.setState({});
                }}
                inactiveSlideOpacity={0.6}
                // shouldOptimizeUpdates
                itemHeight={40}
                data={['09 ноября', 'Вчера', 'Сегодня', 'Завтра', '13 ноября']}
                firstItem={2}
                renderItem={this._renderItem}
                sliderWidth={400}
                itemWidth={100}
              />
            </View>
            <AnimatedCircularProgress
              rotation={130}
              lineCap={'round'}
              size={WINDOW_HEIGHT / 5}
              width={10}
              fill={calories !== 0 ? (calories / 4000) * 100 : 0}
              tintColor="#F46F22"
              onAnimationComplete={() => console.log('onAnimationComplete')}
              backgroundColor="rgba(244, 111, 34, 0.2)">
              {() => (
                <View style={styles.textCircleContainer}>
                  <Text style={styles.calories}>{calories}</Text>
                  <Text style={styles.cCal}>кКал</Text>
                  <Text style={styles.goal}>Цель</Text>
                </View>
              )}
            </AnimatedCircularProgress>
            <View style={styles.rowIconsContainer}>
              <SubstanceElement
                imagePath={iconImages.prot}
                middleTitle={'БЕЛКИ'}
                bottomTitle={'118/210 г'}
              />
              <SubstanceElement
                imagePath={iconImages.uglevod}
                middleTitle={'УГЛЕВОДЫ'}
                bottomTitle={'118/210 г'}
              />
              <SubstanceElement
                imagePath={iconImages.fats}
                middleTitle={'ЖИРЫ'}
                bottomTitle={'118/210 г'}
              />
            </View>
            <View style={styles.containerListFood}>
              <SectionList
                showsVerticalScrollIndicator={false}
                style={styles.listDish}
                sections={DATA}
                keyExtractor={(item, index) => item + index}
                renderItem={({item, index}) => (
                  <LinearGradient
                    key={index}
                    useAngle
                    angle={240}
                    locations={[0, 1]}
                    colors={['rgba(255, 255, 255, 0.5);', '#DADADA']}
                    style={styles.gradient}>
                    <Text style={styles.listFoodItem}>{item}</Text>
                    <Text style={styles.listCal}>225 Ккал</Text>
                  </LinearGradient>
                )}
                renderSectionHeader={({section: {title}}) => (
                  <Text style={styles.listEatingTimeHeader}>{title}</Text>
                )}
              />
            </View>
          </View>
        </ScrollView>
        <BottomButton
          title={'Добавить блюдо'}
          backFunction={() => this.props.navigation.navigate('AddDish')}
        />
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    marginBottom: 60,
  },
  textCircleContainer: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  miniHeader: {
    fontSize: fontSize14,
    fontFamily: PfDinDisplayProBold,
    color: '#F46F22',
    paddingTop: 18,
  },
  calories: {
    fontFamily: PfDinDisplayProBold,
    fontSize: fontSize28,
    color: '#151C26',
  },
  cCal: {
    fontFamily: PfDinDisplayPro,
    fontSize: fontSize18,
    color: '#151C26',
  },
  goal: {
    fontFamily: PfDinDisplayPro,
    fontSize: fontSize12,
    color: 'rgba(21, 28, 38, 0.5)',
  },
  rowIconsContainer: {
    height: '10%',
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginTop: 32,
    marginBottom: 16,
  },
  iconTitleContainer: {
    justifyContent: 'center',
    flexDirection: 'column',
    alignItems: 'center',
  },
  iconImage: {
    width: fontSize28,
    height: fontSize28,
  },
  middleTitle: {
    fontSize: fontSize12,
    fontFamily: SfUiDisplayRegular,
    textAlign: 'center',
    color: 'rgba(21, 28, 38, 0.38);',
    paddingTop: 8,
  },
  bottomTitle: {
    fontFamily: SfUiDisplayRegular,
    textAlign: 'center',
    fontSize: fontSize16,
  },
  gradient: {
    marginTop: 8,
    paddingVertical: 18,
    paddingHorizontal: 8,
    borderRadius: 10,
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  listDish: {
    marginBottom: WINDOW_HEIGHT / 11.5,
    backgroundColor: '#FAFAFA',
    width: '100%',
    paddingHorizontal: 16,
    borderTopLeftRadius: 16,
    borderTopRightRadius: 16,
  },
  containerListFood: {
    backgroundColor: '#FAFAFA',
    paddingTop: 6,
    width: '100%',
    flex: 1,
    borderRadius: 16,
  },
  listCal: {
    paddingLeft: 16,
    color: '#0FD8D8',
    fontSize: fontSize18,
  },
  listFoodItem: {
    paddingLeft: 16,
    fontSize: fontSize16,
    fontFamily: PfDinDisplayPro,
  },
  listEatingTimeHeader: {
    fontFamily: PfDinDisplayProBold,
    fontSize: 18,
    paddingVertical: 18,
  },
});

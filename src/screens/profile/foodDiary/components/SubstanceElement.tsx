import {Image, StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {
  fontSize12,
  fontSize16,
  fontSize28,
  SfUiDisplayRegular,
} from '../../../../share/consts';

export const SubstanceElement = ({
  imagePath,
  middleTitle,
  bottomTitle,
}: {
  imagePath: object;
  middleTitle: string;
  bottomTitle: string;
}) => {
  return (
    <View style={styles.iconTitleContainer}>
      <Image
        resizeMode={'contain'}
        style={styles.iconImage}
        source={imagePath}
      />
      <Text style={styles.middleTitle}>{middleTitle}</Text>
      <Text style={styles.bottomTitle}>{bottomTitle}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  iconTitleContainer: {
    justifyContent: 'center',
    flexDirection: 'column',
    alignItems: 'center',
  },
  iconImage: {
    width: fontSize28,
    height: fontSize28,
  },
  middleTitle: {
    fontSize: fontSize12,
    fontFamily: SfUiDisplayRegular,
    textAlign: 'center',
    color: 'rgba(21, 28, 38, 0.38);',
    paddingTop: 8,
  },
  bottomTitle: {
    fontFamily: SfUiDisplayRegular,
    textAlign: 'center',
    fontSize: fontSize16,
  },
});

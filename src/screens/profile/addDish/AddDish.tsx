import React, {Component} from 'react';
import {
  PanResponder,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import Header from '../../../share/components/Header';
import MaterialIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import EntypoIcon from 'react-native-vector-icons/Entypo';

import {
  fontSize14,
  fontSize28,
  fontSize34,
  PfDinDisplayProBold,
} from '../../../share/consts';
import {NavigationProps} from '../../../share/interfaces';
import DropDownList from './components/DropDownList';
import store from '../../../stores';
import {TextInputCustom} from '../../../share/components/TextInpurCustom';
import {HeaderTitle} from './components/HeaderTitle';

interface IState {
  calories: number;
  dish: string;
  dishBorder: string;
  caloriesBorder: string;
}

const borderColor = 'rgba(0, 0, 0, 0.12)';
export default class AddDish extends Component<NavigationProps, IState> {
  constructor(props: any) {
    super(props);
    this.state = {
      calories: 0,
      dish: '',
      dishBorder: borderColor,
      caloriesBorder: borderColor,
    };
  }

  onFocus(inputName: string) {
    // @ts-ignore
    this.setState({[inputName]: '#F46F22'});
  }

  onBlur(inputName: string) {
    // @ts-ignore
    this.setState({[inputName]: borderColor});
  }

  render() {
    const {eating, showMenu, setDishInfo} = store.addDishStore;
    const {dish, calories, dishBorder, caloriesBorder} = this.state;
    return (
      <ScrollView
        onTouchStart={() => {
          showMenu(true);
        }}>
        <Header
          style={styles.header}
          marginTopHeader={56}
          headerLeft={
            <TouchableOpacity
              style={{paddingRight: 16}}
              onPress={() => {
                this.props.navigation.goBack();
              }}>
              <EntypoIcon
                name="chevron-small-left"
                size={fontSize34}
                color="#151C26"
              />
            </TouchableOpacity>
          }
          title={'Добавить блюдо'}
          headerRight={
            <TouchableOpacity
              style={{paddingRight: 16}}
              onPress={() => {
                setDishInfo(calories, dish);
                this.props.navigation.goBack();
              }}>
              <MaterialIcon name="check" size={fontSize28} color="#F46F22" />
            </TouchableOpacity>
          }
        />
        <View style={{paddingHorizontal: 16}}>
          <View
            style={{
              flexDirection: 'column',
              paddingTop: 28,
            }}>
            <HeaderTitle text={'Выберите категорию'} style={{paddingTop: 28}} />
            <Text style={styles.miniHeader}>Категория</Text>
            <DropDownList items={eating} />
          </View>
          <HeaderTitle text={'Введите название'} style={{paddingTop: 28}} />
          <View
            style={{
              height: 95,
              paddingTop: 16,
              borderBottomWidth: 1,
              borderBottomColor: dishBorder,
            }}>
            <Text style={styles.miniHeader}>Блюдо</Text>
            <TextInputCustom
              onBlur={() => this.onBlur('dishBorder')}
              onFocus={() => this.onFocus('dishBorder')}
              onChangeText={(text: string) => {
                this.setState({dish: text});
              }}
              value={dish}
            />
          </View>
          <HeaderTitle
            text={'Заполните калорийность'}
            style={{paddingTop: 28}}
          />
          <View
            style={{
              height: 95,
              paddingTop: 16,
              borderBottomWidth: 1,
              borderBottomColor: caloriesBorder,
            }}>
            <Text style={styles.miniHeader}>Калории</Text>
            <TextInputCustom
              onBlur={() => this.onBlur('caloriesBorder')}
              onFocus={() => this.onFocus('caloriesBorder')}
              keyboardType="numeric"
              onChangeText={(text: number) => {
                this.setState({
                  calories: text,
                });
              }}
              value={calories ? calories.toString() : null}
            />
          </View>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    borderBottomWidth: 1,
    borderBottomColor: 'rgba(0, 0, 0, 0.12)',
  },
  miniHeader: {
    fontSize: fontSize14,
    fontFamily: PfDinDisplayProBold,
    color: '#F46F22',
    paddingTop: 18,
  },
});

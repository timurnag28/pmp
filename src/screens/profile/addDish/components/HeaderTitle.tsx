import {StyleProp, Text, View, ViewStyle} from 'react-native';
import React from 'react';
import {fontSize18, PfDinDisplayProBold} from '../../../../share/consts';

export const HeaderTitle = ({
  text,
  style,
}: {
  text: string;
  style?: StyleProp<ViewStyle>;
}) => {
  return (
    <View style={style}>
      <Text style={{fontFamily: PfDinDisplayProBold, fontSize: fontSize18}}>
        {text}
      </Text>
    </View>
  );
};

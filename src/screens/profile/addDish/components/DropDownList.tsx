import React, {Component} from 'react';
import {Text, View, ViewStyle, Animated, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import {
  fontSize18,
  fontSize20,
  PfDinDisplayPro,
} from '../../../../share/consts';
import store from '../../../../stores';
import {observer} from 'mobx-react';
interface IDropDownListProps {
  items: [string, string, string];
  style?: ViewStyle;
}

@observer
export default class DropDownList extends Component<IDropDownListProps> {
  render() {
    const {items, style} = this.props;

    const {
      selectedEating,
      animatedValue,
      selectEating,
      showMenu,
    } = store.addDishStore;
    const viewHeight = animatedValue.interpolate({
      inputRange: [0, 0.5, 1],
      outputRange: [0, 75, 130],
    });
    return (
      <View style={[{flexDirection: 'column'}, style]}>
        <TouchableOpacity
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            borderBottomWidth: 1,
            borderBottomColor: 'rgba(0, 0, 0, 0.12)',
            height: 46,
          }}
          activeOpacity={1}
          onPress={() => {
            showMenu();
          }}>
          <Text style={{fontSize: fontSize18, fontFamily: PfDinDisplayPro}}>
            {selectedEating}
          </Text>
          <Icon name="md-arrow-dropdown" size={fontSize20} color="#000000" />
        </TouchableOpacity>
        <Animated.View
          style={{
            opacity: animatedValue,
            position: 'absolute',
            right: 0,
            left: 0,
            top: 36,
            height: viewHeight,
            backgroundColor: '#fafafa',
            zIndex: 50,
            elevation: 2,
            padding: 8,
          }}>
          {items.map(item => {
            return (
              <TouchableOpacity
                key={item}
                style={{paddingVertical: 8}}
                onPress={() => {
                  selectEating(item);
                  showMenu();
                }}>
                <Text
                  style={{
                    fontSize: fontSize18,
                    fontFamily: PfDinDisplayPro,
                  }}>
                  {item}
                </Text>
              </TouchableOpacity>
            );
          })}
        </Animated.View>
      </View>
    );
  }
}

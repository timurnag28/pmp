import React, {Component} from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import store from '../../stores';
import {NavigationProps} from '../../share/interfaces';

export default class Profile extends Component<NavigationProps> {
  render() {
    const {menuItems} = store.profileStore;
    return (
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        {menuItems.map((item, i) => {
          return (
            <TouchableOpacity
              key={i}
              onPress={() => {this.props.navigation.navigate(item.screen); console.log(item.screen)}}>
              <Text>{item.title}</Text>
            </TouchableOpacity>
          );
        })}
      </View>
    );
  }
}

import React, {Component} from 'react';
import {
  ImageBackground,
  ScrollView,
  SectionList,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import Header from '../../share/components/Header';
import {
  fontSize14,
  fontSize16,
  fontSize18,
  fontSize28,
  fontSize34,
  PfDinDisplayPro,
  PfDinDisplayProBold,
  SfUiDisplayRegular,
  WINDOW_HEIGHT,
} from '../../share/consts';
import EntypoIcon from 'react-native-vector-icons/Entypo';
import {NavigationProps} from '../../share/interfaces';
// @ts-ignore
import Carousel from 'react-native-snap-carousel';
import LinearGradient from 'react-native-linear-gradient';
// @ts-ignore
import {PieChart} from 'react-native-svg-charts';

const DATA = [
  {
    title: 'Октябрь 2019',
    data: [
      'Персональная тренировка 55 минут',
      'СПА процедуры',
      'Посещение косметолога',
    ],
  },
];

const monthNames = [
  'Январь',
  'Февраль',
  'Март',
  'Апрель',
  'Май',
  'Июнь',
  'Июль',
  'Август',
  'Сентябрь',
  'Октябрь',
  'Ноябрь',
  'Декабрь',
];
interface IState {
  activeElement: number;
  selectedSlice: {label: string; value: number};
  labelWidth: number;
}
export default class Statistics extends Component<NavigationProps, IState> {
  currIndex = 0;
  constructor(props: any) {
    super(props);
    this.state = {
      activeElement: 0,
      selectedSlice: {
        label: '',
        value: 0,
      },
      labelWidth: 0,
    };
  }

  componentDidMount() {
    // @ts-ignore
    this.currIndex = this._carousel.currentIndex;
  }

  _renderItem = ({item, index}: {item: string; index: number}) => {
    return (
      <View
        style={{
          backgroundColor: index === this.currIndex ? '#F46F22' : 'transparent',
          borderRadius: 30,
          paddingVertical: 8,
        }}>
        <Text
          style={{
            color: index === this.currIndex ? '#FAFAFA' : '#151C26',
            textAlign: 'center',
            fontFamily: PfDinDisplayPro,
            fontSize: fontSize14,
          }}>
          {item}
        </Text>
      </View>
    );
  };

  render() {
    const {selectedSlice} = this.state;
    const {label} = selectedSlice;
    const keys1 = ['Пилатес', 'Силовая', 'Beauty'];
    const values = [50, 20, 40];
    const colors = ['#F46F22', '#151C26', '#0FD8D8'];
    const data = keys1.map((key, index) => {
      return {
        key,
        value: values[index],
        svg: {fill: colors[index]},
        arc: {
          outerRadius: 70 + values[index] + '%',
          padAngle: label === key ? 0.1 : 0,
        },
        onPress: () =>
          this.setState({selectedSlice: {label: key, value: values[index]}}),
      };
    });

    return (
      <ImageBackground
        style={{
          width: '100%',
          height: '100%',
        }}
        source={require('../../../assets/images/backManImage.png')}>
        <Header
          style={styles.header}
          marginTopHeader={WINDOW_HEIGHT / 16}
          headerLeft={
            <TouchableOpacity
              style={{paddingRight: 16}}
              onPress={() => {
                this.props.navigation.goBack();
              }}>
              <EntypoIcon
                name="chevron-small-left"
                size={fontSize34}
                color="#151C26"
              />
            </TouchableOpacity>
          }
          title={'Статистика и история посещений'}
        />
        <ScrollView>
          <View style={{flex: 1, alignItems: 'center'}}>
            <View style={{height: WINDOW_HEIGHT / 8}}>
              <Carousel
                shouldOptimizeUpdates={true}
                enableMomentum={true}
                activeSlideOffset={10}
                callbackOffsetMargin={20}
                ref={(c: any) => {
                  // @ts-ignore
                  this._carousel = c;
                }}
                onBeforeSnapToItem={(currentIndex: number) => {
                  this.currIndex = currentIndex;
                  this.setState({});
                }}
                inactiveSlideOpacity={0.6}
                itemHeight={40}
                data={monthNames}
                firstItem={2}
                renderItem={this._renderItem}
                sliderWidth={400}
                itemWidth={100}
              />
            </View>
            <View style={{width: '100%'}}>
              <PieChart
                style={{height: WINDOW_HEIGHT / 6, width: '100%'}}
                outerRadius={'80%'}
                innerRadius={'25%'}
                data={data}
              />
            </View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-around',
                width: '90%',
                marginVertical: 16,
              }}>
              <CustomTitle text={'Пилатес (8)'} background={'#F46F22'} />
              <CustomTitle text={'Силовая (8)'} background={'#1D1C1F'} />
              <CustomTitle text={'Beauty (6)'} background={'#0FD8D8'} />
            </View>
            <View style={styles.containerList}>
              <SectionList
                scrollEnabled={false}
                showsVerticalScrollIndicator={false}
                style={styles.list}
                sections={DATA}
                keyExtractor={(item, index) => item + index}
                renderItem={({item, index}) => (
                  <LinearGradient
                    key={index}
                    useAngle
                    angle={240}
                    locations={[0, 1]}
                    colors={['rgba(255, 255, 255, 0.5);', '#DADADA']}
                    // @ts-ignore
                    style={styles.gradientListItems}>
                    <View style={{flexDirection: 'column', paddingLeft: 16}}>
                      <Text
                        style={{
                          fontSize: fontSize16,
                          fontFamily: PfDinDisplayPro,
                        }}>
                        {item}
                      </Text>
                      <Text style={{color: 'rgba(30, 32, 34, 0.54)'}}>
                        11.10.2019
                      </Text>
                    </View>
                    <EntypoIcon name="leaf" size={fontSize28} color="#0FD8D8" />
                  </LinearGradient>
                )}
                renderSectionHeader={({section: {title}}) => (
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                    }}>
                    <Text
                      style={{
                        fontFamily: PfDinDisplayProBold,
                        fontSize: 18,
                        paddingVertical: 18,
                      }}>
                      {title}
                    </Text>
                  </View>
                )}
              />
            </View>
          </View>
        </ScrollView>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    // borderBottomWidth: 1,
    // borderBottomColor: 'rgba(0, 0, 0, 0.12)',
  },
  list: {
    marginBottom: WINDOW_HEIGHT / 11.5,
    backgroundColor: '#FAFAFA',
    width: '100%',
    paddingHorizontal: 16,
    borderTopLeftRadius: 16,
    borderTopRightRadius: 16,
  },
  containerList: {
    backgroundColor: '#FAFAFA',
    width: '100%',
    flex: 1,
    borderRadius: 16,
  },

  cCal: {
    paddingLeft: 16,
    color: '#0FD8D8',
    fontSize: fontSize18,
  },
  listItem: {
    paddingLeft: 16,
    fontSize: fontSize16,
    fontFamily: PfDinDisplayPro,
  },
  gradientListItems: {
    marginTop: 8,
    paddingVertical: 18,
    paddingHorizontal: 8,
    borderRadius: 10,
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  listEatingTimeHeader: {
    fontFamily: PfDinDisplayProBold,
    fontSize: 18,
    paddingVertical: 18,
  },
});

const CustomTitle = ({
  text,
  background,
}: {
  text: string;
  background?: string;
}) => {
  return (
    <View style={{flexDirection: 'row', alignItems: 'center'}}>
      <View
        style={{
          height: 8,
          width: 8,
          borderRadius: 16,
          backgroundColor: background,
          marginRight: 8,
        }}
      />
      <Text
        style={{
          fontFamily: SfUiDisplayRegular,
          fontSize: fontSize14,
        }}>
        {text}
      </Text>
    </View>
  );
};

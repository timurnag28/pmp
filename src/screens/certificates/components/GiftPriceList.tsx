import React, {Component} from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {
  fontSize18,
  PfDinDisplayPro,
  WINDOW_HEIGHT,
} from '../../../share/consts';
import Icon from 'react-native-vector-icons/Fontisto';
import store from '../../../stores';
import {Observer, observer} from 'mobx-react';
import AntDesignIcon from 'react-native-vector-icons/AntDesign';

@observer
export default class GiftPriceList extends Component {
  render() {
    const {showPricesList, showPricesListFunction} = store.certificatesStore;
    return (
      <View style={{paddingBottom: 32}}>
        <TouchableOpacity
          onPress={() => {
            showPricesListFunction();
          }}
          style={styles.selectPriceTitleContainer}>
          <Text style={styles.selectPriceTitle}>Выберите номинал</Text>
          <AntDesignIcon
            color={'rgba(0,0,0,.5)'}
            name={showPricesList ? 'up' : 'down'}
            size={fontSize18}
          />
        </TouchableOpacity>
        {showPricesList ? (
          <View style={{paddingBottom: 64}}>{listPrices}</View>
        ) : null}
      </View>
    );
  }
}

const listPrices = store.certificatesStore.giftPrices.map((item, index) => {
  return (
    <Observer key={index}>
      {() => (
        <TouchableOpacity
          onPress={() => {
            store.certificatesStore.selectPrice(item.price);
          }}
          style={styles.renderItemContainer}>
          <View key={item.price} style={styles.iconContainer}>
            {store.certificatesStore.selectedPrice === item.price ? (
              <Icon
                name={'radio-btn-active'}
                size={fontSize18}
                color={'#FF9D04'}
              />
            ) : (
              <Icon
                name={'radio-btn-passive'}
                size={fontSize18}
                color={'rgba(6,6,12,0.12)'}
              />
            )}
          </View>
          <View style={styles.priceContainer}>
            <Text style={styles.price}>{`${item.price} ₽`}</Text>
          </View>
        </TouchableOpacity>
      )}
    </Observer>
  );
});

const styles = StyleSheet.create({
  renderItemContainer: {
    paddingTop: 32,
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
  priceContainer: {
    borderBottomColor: 'rgba(0,0,0,0.12)',
    borderBottomWidth: 1,
    flex: 6,
    height: WINDOW_HEIGHT / 15,
  },
  price: {
    fontSize: fontSize18,
    fontFamily: PfDinDisplayPro,
  },
  iconContainer: {
    flex: 1,
    height: WINDOW_HEIGHT / 15,
    paddingTop: 1,
  },
  selectPriceTitleContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingTop: 36,
  },
  selectPriceTitle: {
    fontSize: fontSize18,
    fontFamily: PfDinDisplayPro,
    color: '#000000',
    lineHeight: 22,
  },
});

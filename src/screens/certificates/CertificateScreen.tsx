import React, {Component} from 'react';
import {
  Alert,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {observer} from 'mobx-react';
import {
  BadScript,
  fontSize16,
  fontSize24,
  fontSize32,
  fontSize34,
  PfDinDisplayPro,
  PfDinDisplayProBold,
  WINDOW_HEIGHT,
} from '../../share/consts';
import Header from '../../share/components/Header';
import BowComponent from '../../share/components/BowComponent';
import BackGroundGradient from '../../share/components/BackGroundGradient';
import GiftPriceList from './components/GiftPriceList';
import store from '../../stores';
import Icon from 'react-native-vector-icons/Ionicons';
import {BottomButton} from '../../share/components/BottomButton';

@observer
export default class CertificateScreen extends Component {
  render() {
    const {showPricesList, addToBasket} = store.certificatesStore;
    return (
      <ScrollView>
        <View style={styles.mainContainer}>
          <Header
            marginTopHeader={56}
            headerLeft={<Text style={styles.leftHeaderTitle}>Сертификаты</Text>}
            headerRight={
              <TouchableOpacity
                style={{}}
                onPress={() => {
                  Alert.alert('...');
                }}>
                <Icon name="ios-menu" size={fontSize32} color="#F46F22" />
              </TouchableOpacity>
            }
          />
          <View style={styles.cardElementsContainer}>
            <BackGroundGradient />
            <BowComponent />
            <View style={styles.cardTitleContainer}>
              <Text style={styles.cardTitle}>Подарочный{'\n'}СЕРТИФИКАТ</Text>
            </View>
          </View>
          <Text style={styles.heading}>
            Подарочные карты в эксклюзивной упаковке
          </Text>
          <Text style={styles.description}>
            Подарочные карты в эксклюзивной упаделали полезные подарки близким и
            искали единомышленников среди друзей, мы подготовили подарочные
            сертификаты — депозит карты вы выбираете сами. Стильно и эксклюзивно
            оформленные карты мы можем доставить курьером в любую удобную для
            вас точку. {'\n'}
            {'\n'}
            Для вашего удобства вы можете выбрать здесь карту с фиксированным
            номиналом и произвести оплату онлайн. После оплаты с вами свяжется
            сотрудник рецепции для уточнения деталей по доставке.
          </Text>
          <GiftPriceList />
        </View>
        {showPricesList ? (
          <BottomButton title={'В КОРЗИНУ'} backFunction={addToBasket} />
        ) : null}
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    paddingHorizontal: 16,
  },
  cardElementsContainer: {
    height: WINDOW_HEIGHT / 4,
    marginTop: 16,
    paddingBottom: 16,
  },
  cardTitleContainer: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  cardTitle: {
    letterSpacing: 3,
    fontFamily: BadScript,
    fontSize: fontSize24,
    textAlign: 'center',
    color: '#FAFAFA',
  },
  heading: {
    paddingTop: 24,
    fontFamily: PfDinDisplayProBold,
    fontSize: 18,
    lineHeight: 24,
    color: '#151C26',
  },
  description: {
    paddingTop: 6,
    fontFamily: PfDinDisplayPro,
    fontSize: fontSize16,
    lineHeight: 24,
  },
  bottomButton: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    height: WINDOW_HEIGHT / 12,
    borderTopLeftRadius: 16,
    borderTopRightRadius: 16,
    backgroundColor: '#F46F22',
    elevation: 6,
  },
  bottomButtonTitle: {
    color: '#FFFFFF',
    fontSize: fontSize16,
    fontFamily: PfDinDisplayPro,
  },
  leftHeaderTitle: {
    fontFamily: PfDinDisplayProBold,
    fontSize: fontSize34,
    lineHeight: 41,
    letterSpacing: 0.41,
    color: '#151C26',
  },
});

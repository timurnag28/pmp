import {action, observable} from 'mobx';
import {Animated} from 'react-native';

class AddDishStore {
  @observable eating: [string, string, string] = ['Завтрак', 'Обед', 'Ужин'];
  @observable selectedEating: string = '';
  @observable onShow: boolean = true;
  @observable animatedValue = new Animated.Value(0);
  @observable dish = '';
  @observable calories = 0;

  @action selectEating = (selectedEating: string) => {
    this.selectedEating = selectedEating;
  };
  @action showMenu = (globalClose = false) => {
    console.log('globalClose ' + globalClose);
    console.log('this.onShow ' + this.onShow);
    if (this.onShow && !globalClose) {
      Animated.timing(this.animatedValue, {
        toValue: 1,
        duration: 100,
        // useNativeDriver: true,
      }).start();
      this.onShow = false;
    } else {
      Animated.timing(this.animatedValue, {
        toValue: 0,
        duration: 0,
        // useNativeDriver: true,
      }).start();
      if (!globalClose) {
        this.onShow = true;
      }
    }
  };

  @action setDishInfo = (valueCalories: number, valueDish: string) => {
    this.calories = valueCalories;
    this.dish = valueDish;
  };
}

const addDishStore = new AddDishStore();
export default addDishStore;

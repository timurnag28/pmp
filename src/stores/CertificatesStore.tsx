import {action, observable} from 'mobx';
import {IGiftPrices} from '../share/interfaces';

class CertificatesStore {
  @observable giftPrices: IGiftPrices[] = [
    {price: 5000},
    {price: 10000},
    {price: 15000},
    {price: 20000},
    {price: 30000},
    {price: 50000},
  ];
  @observable selectedPrice: number | undefined;
  @observable showPricesList: boolean = false;

  @action
  selectPrice = (item: number) => {
    this.selectedPrice = item;
  };

  @action
  showPricesListFunction = () => {
    this.showPricesList = !this.showPricesList;
  };

  @action
  addToBasket = () => {
    alert(
      this.selectedPrice ? `${this.selectedPrice}` : `Вы ничего не выбрали.`,
    );
  };
}

const certificatesStore = new CertificatesStore();
export default certificatesStore;

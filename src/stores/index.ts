import certificatesStore from './CertificatesStore';
import profileStore from './ProfileStore';
import addDishStore from './AddDishStore';

const store = {
  certificatesStore,
  profileStore,
  addDishStore,
};

export default store;

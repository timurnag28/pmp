import {action, observable} from 'mobx';
import {IMenuItems} from '../share/interfaces';

class ProfileStore {
  @observable menuItems: IMenuItems[] = [
    {title: 'Мое расписание', screen: 'Schedule'},
    {title: 'Статистика и история посещений', screen: 'Statistics'},
    {title: 'Дневник питания', screen: 'FoodDiary'},
    {title: 'Сертификаты', screen: 'CertificatesScreen'},
  ];
}

const profileStore = new ProfileStore();
export default profileStore;
